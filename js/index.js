"use strict"
// Переменные в  Javascript оглашаться с помощью ключевого слова
// Var , let, const.
// prompt  в отличии от confirm может от пользователя получить данные
// Неявное преобразование типов –значения могут менять свой тип автоматически 
// ==

let name = "Александр";
let admin;
admin = name;
console.log(admin);

let day = 5;
let seconds = day * 24 * 60 * 60;
console.log(seconds + "секунд");

let answer  = prompt ( "введите любое значение");
console.log(answer);